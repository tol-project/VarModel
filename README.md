# VAR Model

This package implement classes to represent VAR and cointegrated VAR
model. The vector error correction (VEC) specification is used to deal
with the cointegrated model. The package contains functions to
identify the order of the VAR and the cointegration rank. For a given
estimated VAR model we can perform stability check, wold MA
decomposition and the computation of the IRF.
